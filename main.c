#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>

#define FREQ2CYCLES(freq) ((freq) ? F_CPU / 128 / freq : 0)
#define MS2TENMS(len) (len / 12)
#define NOTE(freq, len) { FREQ2CYCLES(freq), MS2TENMS(len) }

#include "songs/clubbed2.h"

volatile uint16_t idx = 0;

register uint8_t cnt asm("r2");

void next(void) {
	if (idx == sizeof(song) / 2) {
		//idx = 26;
		idx = 0;
	}

	OCR1C = pgm_read_byte(&song[idx][0]);
	cnt = pgm_read_byte(&song[idx++][1]);

	if (OCR1C) {
		TCCR1 |= _BV(COM1A0);
		PORTB ^= _BV(PB0);
	}
}

int main(void) {
	DDRB |= _BV(DDB0);
	DDRB |= _BV(DDB1);

	cli();

	next();

	// timer 0
	TCCR0A |= _BV(WGM01);
	OCR0A = 0x4D; // 10 ms
    TIMSK |= _BV(OCIE0A);
	TCCR0B |= _BV(CS00) | _BV(CS02);


	// timer 1
	TCCR1 |= _BV(CTC1) | _BV(COM1A0) | _BV(CS13);


	sei();

    while (1) {
        sleep_mode();
    }

    return 0;
}

ISR(TIM0_COMPA_vect) {
	--cnt;

	if (cnt == 1) {
		TCCR1 &= ~_BV(COM1A0);
	}

	if (cnt == 0) {
		next();
	}
}
